#!/bin/bash
#Version 1.5 20.02.2024

if [[ $(id -u) -eq 0 ]]
	then
		echo ""
		echo "Das Skript bitte als $(tput bold)SIDADM$(tput sgr 0) ausführen!"
		echo ""
	exit
fi

echo ""
read -rp "$(tput bold)$(tput setaf 3)Wie lautet die SID des Systems? $(tput sgr 0)" SID
echo ""

read -rp "$(tput bold)$(tput setaf 3)Wie lautet die SID des Transportcontrollers? $(tput sgr 0)" TC_SID
echo ""

echo "Die SID des Zielsystems lautet: $(tput bold)$SID$(tput sgr 0)"
echo "Die SID des Transportcontrollers lautet: $(tput bold)$TC_SID$(tput sgr 0)"
echo ""
read -rp "$(tput bold)$(tput setaf 3)Mit ENTER quittieren, STRG+C zum Abbrechen$(tput sgr 0)"
echo ""

echo "Sämtliche Transportaufträge, die in der Datei $(tput bold)transporte.txt$(tput sgr 0) angeführt sind, werden an die Queue angehängt. (Form: $(tput bold)SID$(tput sgr 0)K$(tput bold)900000$(tput sgr 0))"
echo ""
read -rp "$(tput bold)$(tput setaf 3)Mit ENTER quittieren, STRG+C zum Abbrechen$(tput sgr 0)"
echo ""

if [ -f transporte.txt ]
then
        echo "$(tput bold)$(tput setaf 2)Importdatei $(tput bold)transporte.txt$(tput sgr 0) gefunden:$(tput sgr 0)"
		#Konvertierung von Windows File Ending auf Linux File Ending
		sed -i.bak 's/\r$//' transporte.txt
        echo ""
else
        echo "$(tput bold)$(tput setaf 1)Importdatei $(tput bold)transporte.txt$(tput sgr 0) nicht gefunden!$(tput sgr 0)"
        echo ""
		exit
fi

cat transporte.txt
echo ""
read -rp "$(tput bold)$(tput setaf 3)Mit ENTER quittieren, STRG+C zum Abbrechen$(tput sgr 0)"
echo ""

if [ -f /usr/sap/trans/bin/TP_DOMAIN_"$TC_SID".PFL ]
then
        echo "$(tput bold)$(tput setaf 2)Transportprofil /usr/sap/trans/bin/TP_DOMAIN_$TC_SID.PFL existiert und wird verwendet.$(tput sgr 0)"
        echo ""
else
        echo "$(tput bold)$(tput setaf 1)Transportprofil /usr/sap/trans/bin/TP_DOMAIN_$TC_SID.PFL existiert nicht. Bitte überprüfen!$(tput sgr 0)"
        echo ""
		exit
fi

#tp addtobuffer "$transport" "$SID" U01 pf=/usr/sap/trans/bin/TP_DOMAIN_"$TC_SID".PFL
#0 – Leave Transport Request in Queue for Later Import
#1 – Import Transport Request again
#2 – Overwrite Originals
#6 – Overwrite Objects in Unconfirmed Repairs

for transport in $(cat transporte.txt);
do
	echo "$transport"
	tp addtobuffer "$transport" "$SID" pf=/usr/sap/trans/bin/TP_DOMAIN_"$TC_SID".PFL | tee -a addtobuffer.log
	echo ""
done

