#!/bin/bash
#Version 1.2 05.03.2024

# Logdatei definieren
echo ""
read -rp "$(tput bold)Welches Log a) import.log b) addtobuffer.log soll ausgewertet werden? $(tput sgr 0)" logauswahl
echo ""
case $logauswahl in
  a)	if [ -f import.log ]; then echo "Datei $(tput setaf 2)import.log$(tput sgr 0) wird ausgewertet."; else echo "$(tput bold)$(tput setaf 1)Datei import.log nicht gefunden. Abbruch.$(tput sgr 0)"; echo ""; exit; fi
		LOG_FILE=import.log ;;
  b)	if [ -f addtobuffer.log ]; then echo "Datei $(tput setaf 2)addtobuffer.log$(tput sgr 0) wird ausgewertet."; else echo "$(tput bold)$(tput setaf 1)Datei addtobuffer.log nicht gefunden. Abbruch.$(tput sgr 0)"; echo ""; exit; fi
		LOG_FILE=addtobuffer.log ;;		
  *)	echo "$(tput setaf 1)Selektion unbekannt: $logauswahl$(tput sgr 0)"
		echo ""
		exit ;;
esac

# Zeilen mit Return Code über 4 finden und speichern
awk -F' ' '{if ($6 > 4) print NR,$0}' $LOG_FILE > zeilen_mit_zeilennummer.txt

grep "return code" zeilen_mit_zeilennummer.txt > zeilen_mit_zeilennummer_gefiltert.txt

awk -F ' ' '{print $1}' zeilen_mit_zeilennummer_gefiltert.txt > fehlerhafte_zeilen.txt

# Anzahl der gefundenen Fehlerzeilen ermitteln
FEHLER_ANZAHL=$(wc -l fehlerhafte_zeilen.txt | awk '{print $1}')

# Wenn Fehler gefunden wurden, diese mit Kontext ausgeben
if [ "$FEHLER_ANZAHL" -gt 0 ]; then
	echo ""
	echo "$(tput bold)$(tput setaf 1)Es wurden in der Datei $FEHLER_ANZAHL Einträge mit Return Code > 4 gefunden:$(tput sgr 0)"
	echo ""
	while read -r zeilennummer; do
		# Zeile vor der Fehlerzeile ausgeben
		let zeile_vorher=$zeilennummer-1
		sed -n "$zeile_vorher"p $LOG_FILE
		# Fehlerzeile ausgeben
		sed -n "$zeilennummer"p $LOG_FILE
		echo ""
	done < fehlerhafte_zeilen.txt
else
	echo ""
	echo "$(tput bold)$(tput setaf 2)Keine Fehler mit Return Code > 4 gefunden.$(tput sgr 0)"
	echo ""
fi

# Datei mit fehlerhaften Zeilen löschen
rm fehlerhafte_zeilen.txt zeilen_mit_zeilennummer.txt zeilen_mit_zeilennummer_gefiltert.txt